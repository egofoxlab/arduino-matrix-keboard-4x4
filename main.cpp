#include <Arduino.h>

//	Соответствие текста клавиши и ее позиции
char keyboard[4][4] = {
		{'1', '2', '3', 'A'},
		{'4', '5', '6', 'B'},
		{'7', '8', '9', 'C'},
		{'*', '0', '#', 'D'}
};
//	Пины строк
uint8_t rows[4] = {5, 6, 7, 8};
//	Пины колонок
uint8_t cols[4] = {9, 10, 11, 12};
int matrix[4][4];
//	Размер клавиатуры
uint8_t keypadSize = 4;
char pressedButtons[16];

/**
 * Показывать матрицу клавиатуры
 */
void showMatrix();

/**
 * Вернуть значение нажатой кнопки
 *
 * @return char
 */
void getKey();

void setup() {
	Serial.begin(9600);
}

void loop() {
	//	Выводим время, чтобы различать новую итерацию loop-па
	Serial.println(millis());

	//	Устанавливаем режим пинов строк как INPUT_PULLUP, то есть устанавливает на вход с подтягивающим резистором
	for (uint8_t row : rows) {
		pinMode(row, INPUT_PULLUP);
	}

	//	А тут начинается вся магия
	//
	//	В верхнем цикле обходим колонки
	for (uint8_t c = 0; c < keypadSize; c++) {
		//	Так как у нас кнопки свзяаны по колонкам и рядам, то подаем LOW уровень.
		//	Если была нажата кнопка, то кнопки, которые свзязаны в ряд, обязательно пропустят низкий сигнал
		//	и мы это зафиксируем
		pinMode(cols[c], OUTPUT);
		digitalWrite(cols[c], LOW);

		//	Проходим ряды, чтобы узнать, была ли нажата кнопка, если да, то она пропускает низский уровень сигнала
		for (uint8_t r = 0; r < keypadSize; r++) {
			matrix[r][c] = !digitalRead(rows[r]);
		}

		//	Ставим режим INPUT, чтобы не сбивать считываение дынных с других кнопок в том случае,
		//	если была нажата кнопка, потому что ряд в матрице будет положительным, что есть ошибкой.
		//	Так как дынные с данной колонки мы уже считали, то "закрываем" ее, а именно высокий уровень.
		pinMode(cols[c], INPUT);
	}

	//	Выводим Матрицу клавиатуры
	showMatrix();
	//	Обновить массив нажатых клавиш
	getKey();

	//	Показать нажатые клавиши
	for (char button : pressedButtons) {
		Serial.print(button);
		Serial.print(" ");
	}

	Serial.println();

	delay(1000);
}

void showMatrix() {
	for (uint8_t r = 0; r < keypadSize; r++) {
		for (uint8_t c = 0; c < keypadSize; c++) {
			Serial.print(matrix[r][c]);
			Serial.print(" ");
		}

		Serial.println();
	}
}

void getKey() {
	unsigned i = 0;

	for (; i < (sizeof(pressedButtons) / sizeof(pressedButtons[0])); i++) {
		pressedButtons[i] = '\0';
	}

	i = 0;

	for (uint8_t r = 0; r < keypadSize; r++) {
		for (uint8_t c = 0; c < keypadSize; c++) {
			if (matrix[r][c]) {
				pressedButtons[i] = keyboard[r][c];
				i++;
			}
		}
	}
}

